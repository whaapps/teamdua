<?php

    require "../../config/connect.php";

    $result = array();
    
    $lihat = mysqli_query($con, "SELECT * FROM produk");
    while ($a = mysqli_fetch_array($lihat)) {
        # code...
        $nilai['nama_produk'] = $a['nama_produk'];
        $nilai['hrg_jual'] = $a['hrg_jual'];
        $nilai['hrg_beli'] = $a['hrg_beli'];
        $nilai['qty_produk'] = $a['qty_produk'];
        
        array_push($result,$nilai);
    }
    echo json_encode($result);
?>