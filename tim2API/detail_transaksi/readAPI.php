<?php

    require "../../config/connect.php";

    $result = array();

    $lihat = mysqli_query($con, "SELECT * FROM detail_transaksi");
    while ($a = mysqli_fetch_array($lihat)) {
        # code...
        $nilai['id'] = $a['id'];
        $nilai['no_transaksi'] = $a['no_transaksi'];
        $nilai['nama_produk'] = $a['nama_produk'];
        $nilai['harga'] = $a['harga'];
        $nilai['qty_beli'] = $a['qty_beli'];

        array_push($result,$nilai);
    }
    echo json_encode($result);
?>
